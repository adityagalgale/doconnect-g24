package com.grp24.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@EnableDiscoveryClient
@SpringBootApplication
public class DoConnectBackendChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoConnectBackendChatApplication.class, args);
	}

}
